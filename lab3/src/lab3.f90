program lab3
    use Environment
    use Models
    use ListProcess
    use ListIo

    implicit none
    character(:), allocatable   :: inputFile, outputFile
    type(Record), pointer       :: List => Null()!, min=>Null()

    inputFile = "../data/input.txt"
    outputFile = "output.txt"
    List => Read_list(inputFile)
    call Output_list(outputFile, List, "Initial List","rewind") 
    call selectionSort(List) 
    !call getMin(List, List%id, min)
    !call swap(List, min%next)
    call Output_list(outputFile, List, "Sorted" ,"append") 
    ! 
    !call getMin(List%next, List%next%id, min)
    !call swap(List%next, min)
    !call Output_list(outputFile, List, "Sorted" // min%id,"append") 
    !
    !call getMin(List%next%next, List%next%next%id, min)
    !call swap(List%next%next, min%next)
    !call Output_list(outputFile, List, "Sorted" // min%id,"append") 
    !
    !call getMin(List%next%next%next, List%next%next%next%id, min)
    !call swap(List%next%next%next, min%next)
    !call Output_list(outputFile, List, "Sorted","append") 
end program lab3
