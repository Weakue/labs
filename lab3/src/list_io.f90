module ListIo
    use Models
    use Environment
    use ListProcess
    
    implicit none

contains
   function Read_list(Input_File) result(List)
      character(*), intent(in)               :: Input_File
      type(Record), pointer  :: List
      integer  In

      open (file=Input_File, encoding=E_, newunit=In)
      List => Read_rec(In)
      close (In)
   end function Read_list

   recursive function Read_rec(In) result(Rec)
      type(Record), pointer               :: Rec
      integer, intent(in)                 :: In
      integer  IO
      
      allocate (Rec)
      read (In, '(i3,a)', iostat=IO) rec%id, rec%surname
    !  call Handle_IO_status(IO, "reading line from file")
      if (IO == 0) then
         Rec%next => Read_rec(In)
      else
         deallocate (Rec)
         nullify (Rec)
      end if
   end function Read_rec

   subroutine Output_list(Output_File, List, ListName, pos)
      character(*), intent(in)   :: Output_File, ListName, pos
      type(record), intent(in)  :: List
      integer  :: Out
      
      open (file=Output_File, encoding=E_, newunit=Out, position = pos)
      write (out, '(a)') ListName
      call Output_record(Out, List)
      close (Out)
   end subroutine Output_list

   recursive subroutine Output_record(Out, rec)
      integer, intent(in)        :: Out
      type(record), intent(in)  :: rec 
      integer  :: IO
      
      write (Out, '(i2,1x,a)', iostat=IO) rec%id, rec%surname
      !call Handle_IO_status(IO, "writing student to file")
      if (Associated(rec%next)) then
         call Output_record(Out, rec%next)
      endif
   end subroutine Output_record

end module ListIo
