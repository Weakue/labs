module ListProcess 
use Models 
use Environment 

implicit none 

contains 

 pure subroutine swap(from, to) 
  type(Record),pointer,intent(inout) :: from, to 
  type(Record),pointer :: tmp 
  if( associated(from%next, to)) then 
    tmp=>from%next 
    from%next=>from%next%next 
    tmp%next=>from 
    from=>tmp 
  else 
    tmp=>to%next 
    to%next=>from%next 
    from%next=>tmp 
    tmp=>to 
    to=>from 
    from=>tmp 
  endif 
 end subroutine swap 
 
 
 pure recursive subroutine selectionSort(list) 
  type(Record), pointer, intent(inout) :: list 
  if(associated(List%next)) then 
    call SelectSwap(list, list, list%next) 
    call selectionSort(list%next) 
  end if 
 end subroutine selectionSort 

 
 pure recursive subroutine SelectSwap(unsorted, min, current) 
  type(Record), pointer, intent(inout) :: unsorted, min, current
  if (associated(current%next)) then 
    if (current%id < min%id) then
       call SelectSwap(unsorted, current, current%next) 
    else 
       call SelectSwap(unsorted, min, current%next) 
    end if 
  else 
    if (current%id < min%id) then
       call Swap(unsorted, current) 
    else 
       call Swap(unsorted, min) 
    end if 
  end if
 end subroutine SelectSwap
end module ListProcess
