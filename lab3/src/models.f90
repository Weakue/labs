module Models
    use Environment
    
    type record
        integer                             :: id
        character(15, kind=CH_) :: surname 
        type(record), pointer               :: next => Null()
    end type record

end module Models
