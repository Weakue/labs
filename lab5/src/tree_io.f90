module TreeIo
    use Models
    use Environment
    use TreeProcess
    
    implicit none

contains
   function Read_tree(Input_File) result(Root)
      character(*), intent(in)               :: Input_File
      type(Node), pointer  :: Root
      integer  In
      Root => Null()
      open (file=Input_File, encoding=E_, newunit=In)
      call Read_Leaf(In, Root)
      close (In)
   end function Read_tree

   recursive subroutine Read_Leaf(In, Root)
      type(Node), pointer               :: Root 
      integer, intent(in)               :: In
      integer                           :: IO, key = 0
      
      read(In, '(i3)', iostat=IO) key
      if(IO==0) then
         call Insert(root,key)
         call Read_Leaf(in, Root)
      end if
   end subroutine Read_Leaf

   recursive subroutine Insert(tree, key)
      type(Node), pointer, intent (inout) :: tree
      integer, intent (out) :: key
      type (Node), pointer :: New
      if (.not. Associated(tree)) then
         Allocate(New)
         New%Key = key
         Tree => New
      else if (key < Tree%Key) then
         if (.not. Associated(Tree%Left)) then
           allocate (New)
           New%Key = key
           Tree%Left => New
        else
           call Insert(Tree%Left, key)
        end if
      else if (key > Tree%Key) then
        if (.not. Associated(Tree%Right)) then
           allocate (New)
           New%Key = key
           Tree%Right => New
        else
           call Insert(Tree%Right, key)
        end if
      end if
   end subroutine Insert


   recursive subroutine Output_tree(Output_File, Root)
      character(*), intent(in)  :: Output_File
      type(Node), pointer       :: Root
      integer Out 
   
      open (file=Output_File, encoding=E_, newunit=Out)
         call Output_Leaf(Out, Root)
      close (Out)
   end subroutine Output_tree

   recursive subroutine Output_Leaf(Out ,tree)
      type(Node), pointer, intent (inout) :: tree
      integer  Out 
       if (Associated(tree%left)) then
         print *, 'left',tree%Key
         call Output_Leaf(Out,tree%left)
       
       end if
      write (Out, "(i0, 1x)", advance = 'no') tree%Key
      
      if (Associated(tree%right)) then
         call Output_Leaf(Out, tree%right )
         print *, 'right',tree%Key
      end if
   end subroutine Output_Leaf
end module TreeIo
