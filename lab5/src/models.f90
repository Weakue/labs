module Models
    use Environment
    
    type Node
       integer                             :: key
       type(Node), pointer               :: left  => Null()
       type(Node), pointer               :: right => Null()
    end type Node

end module Models
