module TreeProcess 
use Models 
use Environment 

implicit none 

contains 

   recursive integer function getSize(tree) result(size)
      type(Node), pointer :: tree
      if(.not. associated(tree)) then
         size=0
      else
         size=getSize(tree%left)+1+getSize(tree%right)
      end if
   end function getSize



   pure recursive subroutine makeTree(tree, amount)
      type(Node), pointer, intent(inout) :: tree
      integer, intent(in)             :: amount
      
      if(amount>1) then
         call increaseDepth(tree)
         call makeTree(tree, amount-1)
      endif
   end subroutine makeTree

   pure recursive subroutine increaseDepth(tree)
      type(Node), pointer, intent(inout) :: tree
      if( associated(tree%left) ) then
         call leftTurn(tree, tree%left)
         if(associated(tree%left)) &
            call increaseDepth(tree%left)
      end if
   end subroutine increaseDepth

   pure subroutine leftTurn(parent, child)
      type(Node), pointer, intent (inout) :: parent, child
      type(Node), pointer                 :: tmp, stableChild
      if(associated(child%right)) then
         stableChild=>Child
         tmp=>child%right
         child%right=>parent
         parent%left=>tmp
         parent=>stableChild
      else
         tmp=>child
         child%right=>parent
         parent%left=>Null()
         parent=>tmp
      end if
   end subroutine leftTurn




   pure subroutine unbalanceTree(tree)
      type(Node), pointer, intent(inout) :: tree
      type(Node), pointer                :: last 
      
      if(associated(tree%right))&
         call makeList(tree%right,tree)
      if(associated(tree%left))&
         call makeList(tree%left,tree)
      last=>tree
      do while(associated (last%left%left))
         last=>last%left
      end do
      last%left%left=>tree
      tree=>last%left
      last%left=>Null()
   end subroutine unbalanceTree


   pure recursive subroutine makeList(tree, root)
      type(Node), pointer, intent (inout) :: tree, root
   
      if (Associated(tree%left)) &
         call makeList(tree%left, root)
         tree%left=>root
         root=>tree 
      if (Associated(tree%right)) &
         call makeList(tree%right, root)
      tree=>Null()
   end subroutine makeList
end module TreeProcess
