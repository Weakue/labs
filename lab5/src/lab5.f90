program lab5
   use Environment
   use Models
   use TreeProcess
   use TreeIo

   implicit none
   character(:), allocatable :: inputFile, outputFile
   type(Node), pointer       :: BST => Null()
   integer :: size
   inputFile = "../data/input.txt"
   outputFile = "output.txt"
   BST => Read_tree(inputFile)
   size = getSize(BST) 
   size = int(log(float(size))/log(2.))
   call unbalanceTree(BST)
   call makeTree(BST,size)


   call Output_tree(outputFile, BST)
    do while (associated(BST) )
      print *,'main', BST%Key
    !  if (associated(BST%Right)) then
    !  print *, BST%right%key, "right"
    ! endif 
      BST=>BST%Left
    end do
end program lab5
