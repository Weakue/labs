
module Group_IO
   use Environment
   use Group_Process

   implicit none
   
contains
   ! Создание неформатированного файла данных.
   subroutine Create_data_file(Input_File, Data_File)
      character(*), intent(in)   :: Input_File, data_file
      
      type(student)              :: stud
      integer                    :: In, Out, IO, i, recl
      character(:), allocatable  :: stud_form
      
      open (file=Input_File, encoding=E_, newunit=In)
      recl = (SURNAME_LEN + INITIALS_LEN + 1)*CH_  + I_
      open (file=Data_File, form='unformatted', newunit=Out, access='direct', recl=recl)
      stud_form = '(3(a, 1x), i4)'
      do i = 1, STUD_AMOUNT
         read (In, stud_form, iostat=IO) stud
         call Handle_IO_status(IO, "reading formatted class list, line " // i)
         
         write (Out, iostat=IO, rec=i) stud
         call Handle_IO_status(IO, "creating unformatted file with class list, record " // i)
      end do
      close (In)
      close (Out)
   end subroutine Create_data_file

   ! Чтение списка класса: фамилии, инициалы, полы и оценки.
   function Read_class_list(Data_File) result(Group)
      type(student)                 Group(STUD_AMOUNT)
      character(*), intent(in)   :: Data_File

      integer In, IO, recl
      
      recl = ((SURNAME_LEN + INITIALS_LEN + 1)*CH_  + I_) * STUD_AMOUNT
      open (file=Data_File, form='unformatted', newunit=In, access='direct', recl=recl)
      read (In, iostat=IO, rec=1) Group
      call Handle_IO_status(IO, "reading unformatted class list")
      close (In)
   end function Read_class_list
 
   ! Вывод списка класса.
   subroutine Output_class_list(Output_File, Group)
      character(*), intent(in)   :: Output_File
      type(student), intent(in)  :: Group(:)

      integer                    :: Out, IO
      character(:), allocatable  :: stud_form
      
      open (file=Output_File, encoding=E_, newunit=Out)
      write (out, '(a)') "Initial list:"
      stud_form = '(3(a, 1x), i4)'
      write (Out, stud_form, iostat=IO) Group
      call Handle_IO_status(IO, "writing class list")
      close (Out)
   end subroutine Output_class_list




   subroutine Output_Aver_Age(output_file, Aver_Age, List_Name)
      character(*)         Output_File, List_Name 
      character(8)         Last_Word  
      intent (in)          Output_File, Aver_Age
      integer(I_)          Aver_Age 
      integer                    :: Out,  IO
      character(:), allocatable  :: format
      if(Aver_Age > 11 .and. Aver_Age < 21) then
         Last_Word = "лет"
      else
         select case (mod(Aver_Age,10))
                case (1)
                  Last_Word = "год"
                case (2:4)
                  Last_Word = "года"
                case default
                  Last_Word = "лет"
         end select
      end if 
      open (file=output_file, encoding=E_, position="append", newunit=Out)
         format = '(a,1x,i5,1x,a)'
         write (Out, format, iostat=IO) "Средний возраст в списке " // List_Name, Aver_Age, Last_Word
         call Handle_IO_status(IO, "writing average age")
      close (Out)
   end subroutine Output_Aver_Age

 !  ! Вывод отсортированного списка класса со средним баллом.
 !  subroutine Output_class_list_with_aver_marks(Output_File, Group, AverMarks)
 !     character(*), intent(in)   :: Output_File
 !     type(student), intent(in)  :: Group(:)
 !     real(R_), intent(in)       :: AverMarks(STUD_AMOUNT)

 !     integer                    :: Out, IO, i
 !     character(:), allocatable  :: stud_form_aver
 !     
 !     open (file=output_file, encoding=E_, position='append', newunit=Out)
 !     write (out, '(/a)') "Sorted list:"
 !     stud_form_aver = '(3(a, 1x) , ' // MARKS_AMOUNT // 'i1, f5.2)'
 !     write (Out, stud_form_aver, iostat=IO) (Group(i), AverMarks(i), i = 1, STUD_AMOUNT)
 !     call Handle_IO_status(IO, "writing sorted class list, record ")
 !     close (Out)
 !  end subroutine Output_class_list_with_aver_marks
end module Group_IO 
