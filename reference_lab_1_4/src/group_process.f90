module Group_Process
   ! Модуль с ЧИСТЫМИ процедурами обработки данных.
   use Environment

   implicit none
   integer, parameter :: STUD_AMOUNT   = 5
   integer, parameter :: SURNAME_LEN   = 15
   integer, parameter :: INITIALS_LEN  = 5

   ! Структура данных для хранения данных о студенте.
   type student
      character(SURNAME_LEN, kind=CH_)    :: Surname              = CH__""
      character(INITIALS_LEN, kind=CH_)   :: Initials             = CH__""
      character(kind=CH_)                 :: Sex                  = CH__""
      integer                             :: Year                 = 0
   end type student
   
contains
   
   pure integer function getAverageAge(Group, Year_Now) result(averAge)
      type(student),intent(in) :: Group(:)
      integer, intent(in)      :: Year_Now
       
     averAge = Year_Now - getBoysAverage(Group,0,0,1) 
       
   end function getAverageAge
   pure integer recursive function getBoysAverage(Array, Year, Count, iterator) result(avAge)
      type(student), intent(in) :: Array(:)
      integer, intent(in) :: Year
      integer, intent(in) :: Count
      integer, intent(in) :: iterator 
      if(Size(Array(iterator:))==1) then
         avAge = Year/Count
      else if(Array(iterator)%Sex== Char(Int(z'041C'), CH_)) then
         avAge = getBoysAverage(Array, Year+Array(iterator)%Year, count+1,iterator+1)
      else 
         avAge = getBoysAverage(Array, Year, count,iterator+1)
      end if
   end function getBoysAverage
   
end module group_process
