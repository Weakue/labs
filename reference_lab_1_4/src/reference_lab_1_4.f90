program reference_lab_1_3
   use Environment
   use Group_Process
   use Group_IO

   implicit none
   character(:), allocatable :: input_file, output_file, data_file
   character(10, kind=1) Year_Now 
   type(student)  :: Group(STUD_AMOUNT) ! Список группы.
   integer(I_)    :: AverAge = 0, Year_Now_Int
   logical, allocatable :: BoysMask(:)
   input_file  = "../data/class.txt"
   output_file = "output.txt"
   data_file   = "class.dat"
   
   call Create_data_file(input_file, data_file)
   
   Group = Read_class_list(data_file)
   
   call Output_class_list(output_file, Group)
   BoysMask = Group%Sex== Char(Int(z'041C'), CH_)
   call Date_And_Time(Year_Now) 
   read(Year_Now(:4), '(i4)') Year_Now_Int
   AverAge = getAverageAge(Group,Year_Now_Int)
   call Output_Aver_Age(output_file,AverAge , "юношей")   
      
      
end program reference_lab_1_3
