program reference_lab_2
   use Environment
   use String_Process
   use String_IO

   implicit none
   character(*), parameter :: F1 = "../data/input.txt", F2 = "output.txt"
   type(Expression),pointer :: expr
   expr => Read_Expr(F1)
   call writeExpression(F2,expr)
   call solveBrackets(expr)
   call solveNots(expr)
   call solve(expr)
   call writeSolution(F2,expr)
   do while(associated(expr)) 
      print *, expr%string, expr%value
      expr=>expr%next
   end do
end program reference_lab_2
