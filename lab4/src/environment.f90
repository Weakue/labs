module environment
   use ISO_Fortran_Env

   implicit none
    
   integer, parameter      :: I_ = INT32                             ! Разновидность типа для целочисленных переменных.
   integer, parameter      :: MAX_LEN = 256  
   integer, parameter      :: R_ = REAL32                            ! Разновидность типа для вещественных переменных.
   integer, parameter      :: C_ = R_                                ! Разновидность типа для компексных переменных.
   integer, parameter      :: CH_= Selected_Char_Kind("ISO_10646")   ! Разновидность типа для символов.
   character(*), parameter :: E_ = "UTF-8"                           ! Кодировка файлов.
   character(Kind=CH_), parameter :: AND   = Char(Int(z'41'), CH_) ! A       
   character(Kind=CH_), parameter :: NOT   = Char(Int(z'4e'), CH_) ! N       
   character(Kind=CH_), parameter :: OR    = Char(Int(z'4f'), CH_) ! O       
   character(Kind=CH_), parameter :: FALSE = Char(Int(z'66'), CH_) ! f       
   character(Kind=CH_), parameter :: TRUE  = Char(Int(z'74'), CH_) ! t       
   character(Kind=CH_), parameter :: LEFT_BRACKET   = Char(Int(z'28'), CH_) ! (       
   character(Kind=CH_), parameter :: RIGHT_BRACKET  = Char(Int(z'29'), CH_) ! )       
   character(Kind=CH_), parameter :: LOW_T = Char(Int(z'75'), CH_) ! )       



   interface operator (//)
      module procedure Int_plus_string
      module procedure String_plus_int
   end interface

contains

   pure function Int_plus_string(int, str) result(res)
      integer, intent(in)                                            :: int
      character(*), intent(in)                                       :: str
      character(len(str)+Max(Floor(Log10(Real(int, real64)))+1, 1))  :: res

      write (res, '(i0, a)') int, str
   end function Int_plus_string

   pure function String_plus_int(str, int) result(res)
      character(*), intent(in)                               :: str
      integer, intent(in)                                    :: int
      character(len(str)+Max(Floor(Log10(Real(int, real64)))+1, 1))  :: res

      write (res, '(a, i0)') str, int
   end function String_plus_int

   ! Обработка статуса ввода/вывода.
   subroutine Handle_IO_status(IO, where)
      integer, intent(in)        :: IO
      character(*), intent(in)   :: where

      integer Out

      Out = OUTPUT_UNIT
      open (Out, encoding=E_)
      select case(IO)
         case(0, IOSTAT_END)
         case(1:)
            write (Out, '(a, i0)') "Error " // where // ": ", IO
         case default
            write (Out, '(a, i0)') "Undetermined behaviour has been reached while " // where // ": ", IO
      end select
      ! close (Out) ! Если не OUTPUT_UNIT.
   end subroutine Handle_IO_status

end module environment
