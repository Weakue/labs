module String_IO
   use Environment
   use String_Process

   implicit none

contains
   function Read_Expr(inputFile) result (expr)
      character(*),intent(in) :: inputFile
      
      type(Variable), pointer        :: vars
      type(Expression), pointer        :: expr
      integer :: In
      !print *,pos
      open (file=inputFile, encoding=E_, newunit=In)
         expr => readExprStr(in)
         vars => readVars(in)
      close (In)
      call insertVars(expr,vars)
   end function Read_Expr

   recursive function readExprStr(in) result(expr)
      type(Expression), pointer  :: expr
      integer, intent(in)  :: in
      integer              :: IO = 0
      allocate(expr)  
      read(in, '(a1)',advance="no", iostat = IO) expr%string
      if(IO == 0) then
         expr%Next => readExprStr(in)       
      else
         deallocate(expr)
         expr=> Null()
      end if
   end function readExprStr

   recursive function readVars(in) result(var)
      type(Variable), pointer  :: var
      character(MAX_LEN,kind=CH_)   :: buffer
      integer, intent(in)      :: in
      integer                  :: IO = 1
      allocate(var)  
      read(in, '(a)', iostat = IO) buffer
      if(IO == 0) then
         var%string = buffer(1:1)
         if(buffer(3:3)==TRUE) &
            var%value = .true.
         if(buffer(3:3)==FALSE) &
            var%value = .false.
         !print *, var%string, var%value
         var%Next => readVars(in)       
      else
         deallocate(var)
         var=> Null()
      end if
   end function readVars

   recursive subroutine insertVars(expr,vars)
      type(Expression), pointer, intent(inout) :: expr
      type(Variable), pointer, intent(inout) :: vars
      if (expr%string == vars%string) then
         expr%value = vars%value
      end if
      if(associated(expr%next)) then
         call insertVars(expr%next,vars)
      end if
      if(associated(vars%next)) then
         call insertVars(expr,vars%next)
      end if   
   end subroutine insertVars

   subroutine writeExpression(outputFile, expr)
      character(*),intent(in) :: outputFile
      type(Expression), pointer     :: expr
      integer                 :: out

      open(file=outputFile, encoding=E_, newunit=out)
          call writeSymbol(out, expr)
      close(out)
   end subroutine writeExpression

   recursive subroutine writeSymbol(out, expr)
      integer, intent(in)         :: out
      type(Expression), intent(in)      :: expr

      write(out, "(a)", advance='no') expr%string
      if (associated(expr%Next)) &
           call writeSymbol(out, expr%Next)
   end subroutine writeSymbol 
   
   subroutine writeSolution(outputFile, expr)
      character(*),intent(in) :: outputFile
      type(Expression), pointer     :: expr
      integer                 :: out

      open(file=outputFile, encoding=E_, newunit=out, position='append')
         if(expr%value) then
            write(out, '(a)') 'Это выражение истинно'
         else
            write(out, '(a)') 'Это выражение ложно'
         end if
      close(out)
   end subroutine writeSolution
end module String_IO 
