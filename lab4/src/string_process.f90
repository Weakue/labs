module String_Process
   
   use Environment

   implicit none

   ! Структура данных для выражения
   type Expression
      character(kind=CH_)     :: String
      logical                 :: value
      type(expression), pointer        :: Next  => Null()
   end type Expression
   ! Структура данных для переменных
   type Variable 
      character(kind=CH_)     :: String
      logical                 :: value
      type(Variable), pointer        :: Next  => Null()
   end type Variable
contains

! процедура, раскрывающая скобки
pure recursive subroutine solveBrackets(expr)
   type(Expression),pointer,intent(inout) :: expr
   type(Expression),pointer :: temp
   if(expr%string == LEFT_BRACKET) then
     if(associated(expr%next)) &
        call solveBrackets(expr%next)
     call solveNots(expr%next)
     call solve(expr%next)
     temp=>expr
     expr => expr%next
     deallocate(temp)
     temp  => expr%next
     expr%next => expr%next%next
     deallocate(temp)
     expr%string = LOW_T
   end if
   if(associated(expr%next)) &
      call solveBrackets(expr%next)
end subroutine solveBrackets

! процедура инвертирующая все NOT
pure recursive subroutine solveNots(expr)
   type(Expression),pointer,intent(inout) :: expr
   type(Expression),pointer :: temp
   !not
   if(expr%string == NOT) then
      if(expr%next%string == LEFT_BRACKET) &
         call solveBrackets(expr%next)
      temp => expr%next
      temp%value = .not. temp%value
      deallocate(expr)
      expr => temp
      if(associated(expr)) then
         if(expr%string /= RIGHT_BRACKET) &
            call solveNots(expr)
      end if
   else 
      if(associated(expr%next)) then
         if(expr%next%string /= RIGHT_BRACKET) &
            call solveNots(expr%next)
      end if
   end if
end subroutine solveNots

!процедура обработки or и and
pure recursive subroutine solve(expr)
   type(Expression),pointer,intent(inout) :: expr
   type(Expression),pointer :: temp
   logical                  :: wrapped
   wrapped = .false.
   if(associated(expr%next)) then
      if(expr%next%string == OR) then
     !   print *,expr%string, expr%next%string,expr%next%next%string
         if(expr%next%next%string == LEFT_BRACKET) &
            call solveBrackets(expr%next%next)
         temp=>expr
         expr%next%value = expr%value .or. expr%next%next%value
         expr%next%string = expr%string
         expr => expr%next
         deallocate(temp)
         temp  => expr%next
         expr%next => expr%next%next
         deallocate(temp)
         wrapped = .true.
      else if(expr%next%string == AND) then
         if(expr%next%next%string == LEFT_BRACKET) &
            call solveBrackets(expr%next%next)
         temp=>expr
         expr%next%value = expr%value .and. expr%next%next%value
         expr%next%string = expr%string
         expr => expr%next
         deallocate(temp)
         temp  => expr%next
         expr%next => expr%next%next
         deallocate(temp)
         wrapped = .true.
      end if
    end if
   
   if(wrapped) then
      if(expr%string /= RIGHT_BRACKET) &
         call solve(expr)
   else
      if(associated(expr%next)) then
         if(expr%next%string /= RIGHT_BRACKET) &
            call solve(expr%next)
      end if
   end if
end subroutine solve
end module String_process
