module String_Process
   
   use Environment

   implicit none

   ! Структура данных для хранения
   type Line
      character(1, kind=CH_)     :: letter
      type(Line), pointer        :: Next  => Null()
   end type Line
contains
   

    pure recursive subroutine getLetterByNumber(str, n, text)
        type(Line), pointer, intent(inout):: text
        type(Line), pointer, intent(out)  :: str
        integer, intent(in)                 :: n
        
        if ((n>1).and.(associated(text%next))) then
            str => text%next
            call getLetterByNumber(str, n - 1, text%next)
        end if
    end subroutine getLetterByNumber
 
   pure subroutine InsertLine(To, From, Position)
      
      type(Line), pointer         :: TempTo, TempFrom
      type(Line), target,intent(inout)      :: From
      type(Line), pointer, intent(inout)   :: To
      integer, intent(in)         :: position
      allocate(TempTo, TempFrom)
      TempTo => To
      call getLetterByNumber(TempTo,Position,TempTo)
       
      TempFrom => From
      do while(Associated(TempFrom%Next%Next))
         TempFrom => TempFrom%Next
      end do 
      TempFrom%Next => TempTo%Next
      TempTo%Next => From
   end subroutine InsertLine
end module String_process
