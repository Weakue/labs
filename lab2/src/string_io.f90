module String_IO
   use Environment
   use String_Process

   implicit none

contains
   function Read_Line(inputFile, pos) result (String)
      character(*),intent(in) :: inputFile, pos
      type(Line), pointer        :: String
      integer :: In
      !print *,pos
      open (file=inputFile, encoding=E_, newunit=In, position = pos)
         read(In,*)
         String => Read_Recursive(in)
      close (In)
   end function Read_Line

   recursive function Read_Recursive(in) result(String)
      character(:,kind=CH_), allocatable :: buf
      type(Line), pointer  :: String
      integer, intent(in)  :: in
      integer              :: IO = 0
      allocate(String)  
      read(in, '(a1)',advance="no", iostat = IO) String%Letter
      !print *, String%Letter 
      if(IO == 0) then
         String%Next => Read_Recursive(in)       
      else
         String%Next => Null()
      end if
   end function Read_Recursive

      
  subroutine writeLine(outputFile, String)
      character(*),intent(in) :: outputFile
      type(Line), pointer     :: String
      integer                 :: out

      open(file=outputFile, encoding=E_, newunit=out)
          call writeLetter(out, String)
      close(out)
  end subroutine writeLine

  recursive subroutine writeLetter(out, String)
      integer, intent(in)         :: out
      type(Line), intent(in)      :: String

      write(out, "(a)", advance='no') String%Letter
      if (associated(String%Next%Next)) &
           call writeLetter(out, String%Next)
  end subroutine writeLetter 
end module String_IO 
