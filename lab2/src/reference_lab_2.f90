program reference_lab_2
   use Environment
   use String_Process
   use String_IO

   implicit none
   character(*), parameter :: F1 = "../data/input.txt", F2 = "output.txt"
   type(Line),pointer :: firstString, secondString
   
      
   firstString  => Read_Line(F1, "rewind")
   secondString => Read_Line(F1, "asis")
   call InsertLine(firstString,secondString,2) ! вставлям вторую в первую
   call writeLine(F2,firstString)   ! печатаем
   
end program reference_lab_2
